import {StatusBar} from 'expo-status-bar';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {TailwindProvider} from 'tailwindcss-react-native';

export default function App() {
    return (
        <TailwindProvider>
            <SafeAreaView>
                <Text>HI there</Text>
                <StatusBar style="auto"/>
            </SafeAreaView>
        </TailwindProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
